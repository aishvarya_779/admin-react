import React from 'react';

class DashboardPage extends React.Component {
	componentDidMount() {
		// this is needed, because InfiniteCalendar forces window scroll
		window.scrollTo(0, 0);
	}

	render() {
		return <h1>Aishvarya</h1>;
	}
}
export default DashboardPage;
