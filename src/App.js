import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import GAListner from './components/GAListner';
import { BrowserRouter, Switch } from 'react-router-dom';
import { LayoutRoute, MainLayout } from './components/Layout';
import DashboardPage from './pages/DashboardPage';

import './styles/main.css';

const getBasename = () => {
	return `/${process.env.PUBLIC_URL.split('/').pop()}`;
};
class App extends Component {
	render() {
		return (
			<BrowserRouter basename={getBasename()}>
				<GAListner>
					<Switch>
						<LayoutRoute exact path="/" layout={MainLayout} component={DashboardPage}>
							{' '}
						</LayoutRoute>
					</Switch>
				</GAListner>
			</BrowserRouter>
		);
	}
}

export default App;
